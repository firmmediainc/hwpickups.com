<?php
	/**
	 * This loop is used to create items for the portfolio archives and also the homepage template.
	 * Any custom functions prefaced with ebor_ are found in /admin/theme_functions.php
	 * First let's declare $post so that we can easily grab everthing needed.
	 */
	 global $post;
	 
	 /**
	  * Next, we need to grab the featured image URL of this post, so that we can trim it to the correct size for the chosen size of this post.
	  */
	 $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
	 
	 /**
	 * Setup variables needed for the gallery
	 */
	 $attachments = get_post_meta( $post->ID, '_ebor_portfolio_gallery_list', true );
	 
	 /**
	  * Leave this portfolio item out if we didn't find a featured image.
	  */
	 if(!( $url[0] ))
	 	return false;
	 	
	 if( get_post_format() == 'video' )
	 	$url[0] = get_post_meta( $post->ID, "_ebor_the_video_1", true );
	 
	 $one = false;
	 if ( is_array( $attachments ) ){
	 	$count = count($attachments) + 1;
	 	$one = ': 1' . __(' of ', 'kubb') . $count;
	 }
	 
	 $fancybody = sprintf( '%s <a href="%s">%s</a>', get_the_excerpt(), get_permalink(), get_option('blog_read_more', 'Read More') );
	 
	 $rel = ( 'yes' == get_option('limit_lightbox', 'no') ) ? get_the_ID() : 'portfolio';
?>

<li id="portfolio-<?php the_ID(); ?>" class="item thumb <?php echo ebor_the_isotope_terms(); ?>">
	<figure>
		<a href="<?php echo $url[0]; ?>" class="fancybox-media" data-rel="<?php echo $rel; ?>" data-title-id="title-<?php the_ID(); ?>">
			<div class="text-overlay">
				<?php the_title('<div class="info">', '</div>'); ?>
			</div>
			<?php the_post_thumbnail('portfolio'); ?>
		</a>
	</figure>
	<div id="title-<?php the_ID(); ?>" class="info hidden">
		<?php the_title('<h2>', $one . '</h2>'); ?>
		<div class="fancybody">
			<p><?php echo $fancybody; ?></p>
		</div>
	</div>
	
	<?php
		/**
		* If we found items, output the gallery.
		* $before and $after change depending on the gallery chosen.
		*/
		if ( is_array( $attachments ) ){
				$i = 1;
				foreach( $attachments as $ID ){
					$i++;
					$attachment = get_post($ID);
					$url = wp_get_attachment_image_src( $attachment->ID, 'full' );
					$one = ': ' . $i . __(' of ', 'kubb') . $count;
					echo '<a href="'. $url[0] .'" class="fancybox-media" data-rel="'. $rel .'" data-title-id="title-'. get_the_ID() . $i .'" style="display:none;"></a>';
					?>
						<div id="title-<?php the_ID(); echo $i; ?>" class="info hidden">
							<?php the_title('<h2>', $one . '</h2>'); ?>
							<div class="fancybody">
								<p><?php echo $fancybody; ?></p>
							</div>
						</div>
					<?php
				}
		
		}
	?>
</li>