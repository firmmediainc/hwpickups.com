<section class="black-wrapper inner ebor-portfolio-carousel">
	<div class="swiper-wrapper"> 
	
		<a class="arrow-left" href="#"></a> 
		<a class="arrow-right" href="#"></a>
		
		<div class="swiper-container gallery">
			<div class="swiper">
				
				<?php 
					if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					
						get_template_part('loop/content-portfolio','carousel');
	
					endwhile;
					else : 
						
						/**
						 * Display no posts message if none are found.
						 */
						get_template_part('loop/content','none');
						
					endif;
				?>
			
			</div>
		</div>
	
	</div>
</section>