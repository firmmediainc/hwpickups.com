<?php

/**
 * Ebor Framework
 * Styles & Scripts Enqueuement
 * @since version 1.0
 * @author TommusRhodus
 */

/**
 * Ebor Load Scripts
 * Properly Enqueues Scripts & Styles for the theme
 * @since version 1.0
 * @author TommusRhodus
 */ 
function ebor_load_scripts() {
	$protocol = is_ssl() ? 'https' : 'http';
	
	/**
	 * Enqueue google fonts
	 */
	wp_enqueue_style( 'ebor-marble-roboto-font', "$protocol://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700italic,700,900,900italic,300italic,300,100italic,100" );
	wp_enqueue_style( 'ebor-marble-roboto-condensed-font', "$protocol://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300" );
	wp_enqueue_style( 'ebor-marble-roboto-slab-font', "$protocol://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" );
	     
	//Enqueue Styles
	wp_enqueue_style( 'ebor-bootstrap', get_template_directory_uri() . '/style/css/bootstrap.css' );
	wp_enqueue_style( 'ebor-fancybox', get_template_directory_uri() . '/style/js/fancybox/jquery.fancybox.css' );
	wp_enqueue_style( 'ebor-fancybox-thumbs', get_template_directory_uri() . '/style/js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.2' );
	
	if (class_exists('Woocommerce'))
		wp_enqueue_style( 'ebor-woocommerce', get_template_directory_uri() . '/style/css/woocommerce.css' );
		
	wp_enqueue_style( 'ebor-style', get_stylesheet_uri() );
	wp_enqueue_style( 'ebor-budicons', get_template_directory_uri() . '/style/type/fonts.css' );
	wp_enqueue_style( 'ebor-custom', get_template_directory_uri() . '/custom.css' );
	
	//Dequeue Styles
	wp_dequeue_style('aqpb-view-css');
	wp_deregister_style('aqpb-view-css');
	
	//Enqueue Scripts
	wp_enqueue_script( 'ebor-bootstrap', get_template_directory_uri() . '/style/js/bootstrap.min.js', array('jquery'), false, true  );
	if ( is_ssl() ) {
	    wp_enqueue_script('ebor-googlemapsapi', 'https://maps-api-ssl.google.com/maps/api/js?sensor=false&v=3.exp', array( 'jquery' ), false, true);
	} else {
	    wp_enqueue_script('ebor-googlemapsapi', 'http://maps.googleapis.com/maps/api/js?sensor=false&v=3.exp', array( 'jquery' ), false, true);
	}
	wp_enqueue_script( 'ebor-plugins', get_template_directory_uri() . '/style/js/plugins.js', array('jquery'), false, true  );
	wp_enqueue_script( 'ebor-scripts', get_template_directory_uri() . '/style/js/scripts.js', array('jquery'), false, true  );
	
	//Enqueue Comments
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	//Dequeue Scripts
	wp_dequeue_script('aqpb-view-js');
	wp_deregister_script('aqpb-view-js');
	
	/**
	 * localize script
	 */
	$script_data = array( 
		'view_larger' => get_option('blog_view_larger','View Larger'),
		'cross_lightbox' => get_option('limit_lightbox', 'no')
	);
	wp_localize_script( 'ebor-scripts', 'wp_data', $script_data );
	
	/**
	 * Pass Theme Options to CSS
	 */
	$highlight = get_option('highlight_colour','#28b8d8');
	$highlightrgb = ebor_hex2rgb( $highlight );
	$highlight_hover = get_option('highlight_hover_colour','#00a1c4');
	$dark_wrapper = get_option('wrapper_background_dark', '#f2f2f2');
	$header_bg = ebor_hex2rgb( get_option('header_bg', '#151515') );
	$header_dropdown_bg = get_option('header_dropdown_bg', '#414141');
	$footer_bg = get_option('footer_bg', '#232323');
	$sub_footer_bg = get_option('sub_footer_bg', '#1e1e1e');
	$black_wrapper = get_option('wrapper_background_black','#272727');
	
	$custom_styles = "
		/**
		 * Woo
		 */
		.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content, .woocommerce-page .widget_price_filter .price_slider_wrapper .ui-widget-content {
		    background: rgba($highlightrgb,0.15);
		}
		
		/**
		 * Wrappers
		 */
		.light-wrapper {
			background: #". get_background_color() .";
		}
		.dark-wrapper {
			background: $dark_wrapper;
		}
		.black-wrapper {
			background: $black_wrapper;
		}
		
		/**
		 * Footer
		 */
		.footer.widget-footer {
			background: $footer_bg;
		}
		
		.sub-footer {
			background: $sub_footer_bg;
		}
		
		.navbar-header {
		    background: rgba($header_bg,0.75);
		}
		
		.navbar.fixed .navbar-header,
		.navbar .dropdown-menu,
		.navbar.fixed .dropdown-menu {
		    background: rgba($header_bg,0.9);
		}
		
		.tp-loader.spinner0,
		#fancybox-loading div {
		    border-left: 3px solid rgba($highlightrgb,.15) !important;
		    border-right: 3px solid rgba($highlightrgb,.15) !important;
		    border-bottom: 3px solid rgba($highlightrgb,.15) !important;
		    border-top: 3px solid rgba($highlightrgb,.8) !important;
		}
		a,
		footer .post-list li .meta h6 a:hover {
		    color: $highlight;
		}
		.navigation a {
		    color: $highlight !important;
		    border: 1px solid $highlight !important;
		}
		.navigation a:hover {
		    background: $highlight_hover !important;
		    border: 1px solid $highlight_hover !important;
		}
		.colored,
		.woocommerce-tabs ul.tabs li.active {
		    color: $highlight
		}
		.color-wrapper,
		.ebor-count,
		.woocommerce .widget_price_filter .ui-slider-horizontal .ui-slider-range, .woocommerce-page .widget_price_filter .ui-slider-horizontal .ui-slider-range,
		.woocommerce span.onsale, .woocommerce-page span.onsale, .woocommerce ul.products li.product .onsale, .woocommerce-page ul.products li.product .onsale,
		.woocommerce .button,
		.added_to_cart,
		.woocommerce .button,
		span.price,
		.woocommerce div.product p.price  {
		    background: $highlight;
		}
		.color-wrapper:hover {
		    background: $highlight_hover;
		}
		.post-title a:hover {
		    color: $highlight
		}
		ul.circled li:before,
		ul.circled li:before, .post-content ul li:before, aside ul li:before {
		    color: $highlight;
		}
		blockquote {
		    background: $highlight;
		}
		.footer a:hover {
		    color: $highlight
		}
		.nav > li > a:hover {
		    color: $highlight;
		}
		.nav > li.current > a {
		    color: $highlight;
		}
		.navbar .nav .open > a,
		.navbar .nav .open > a:hover,
		.navbar .nav .open > a:focus {
		    color: $highlight;
		}
		.navbar .dropdown-menu > li > a:hover,
		.navbar .dropdown-menu > li > a:focus,
		.navbar .dropdown-submenu:hover > a,
		.navbar .dropdown-submenu:focus > a,
		.navbar .dropdown-menu > .active > a,
		.navbar .dropdown-menu > .active > a:hover,
		.navbar .dropdown-menu > .active > a:focus,
		.navbar-nav > li.current-menu-parent > a {
		    color: $highlight;
		}
		.btn,
		.parallax .btn-submit,
		.btn-submit,
		input[type='submit'] {
		    background: $highlight;
		}
		.btn:hover,
		.btn:focus,
		.btn:active,
		.btn.active,
		.parallax .btn-submit:hover,
		input[type='submit']:focus,
		input[type='submit']:hover,
		.woocommerce .button:hover,
		.added_to_cart:hover,
		.woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle {
		    background: $highlight_hover;
		}
		.black-wrapper .btn:hover {
		    border: 1px solid $highlight;
		    color: $highlight !important;
		    background: none;
		}
		.meta a:hover,
		.footer-meta a:hover {
		    color: $highlight
		}
		.more {
		    border-bottom: 1px solid $highlight
		}
		.tooltip-inner {
		    background-color: $highlight;
		}
		.tooltip.top .tooltip-arrow,
		.tooltip.top-left .tooltip-arrow,
		.tooltip.top-right .tooltip-arrow {
		    border-top-color: $highlight
		}
		.tooltip.right .tooltip-arrow {
		    border-right-color: $highlight
		}
		.tooltip.left .tooltip-arrow {
		    border-left-color: $highlight
		}
		.tooltip.bottom .tooltip-arrow,
		.tooltip.bottom-left .tooltip-arrow,
		.tooltip.bottom-right .tooltip-arrow {
		    border-bottom-color: $highlight
		}
		figure a .text-overlay {
		    background: $highlight;
		    background: rgba($highlightrgb,0.90);
		}
		.icon-overlay a .icn-more {
		    background: $highlight;
		    background: rgba($highlightrgb,0.93);
		}
		.filter ul li a:hover,
		.filter ul li.current a,
		.filter ul li a.active {
		    color: $highlight
		}
		.panel-title > a:hover {
		    color: $highlight
		}
		.progress-list li em {
		    color: $highlight;
		}
		.progress.plain {
		    border: 1px solid $highlight;
		}
		.progress.plain .bar {
		    background: $highlight;
		}
		.tp-caption a {
		    color: $highlight
		}
		.services-1 .icon i.icn {
		    color: $highlight;
		}
		.services-2 i {
		    color: $highlight;
		}
		.tabs-top .tab a:hover,
		.tabs-top .tab.active a {
		    color: $highlight
		}
		.pagination ul > li > a,
		.pagination ul > li > span {
		    background: $highlight;
		}
		.pagination ul > li > a:hover,
		.pagination ul > li > a:focus,
		.pagination ul > .active > a,
		.pagination ul > .active > span {
		    background: $highlight_hover;
		}
		.sidebox a:hover,
		#comments .info h2 a:hover,
		#comments a.reply-link:hover,
		.pricing .plan h4 span {
		    color: $highlight
		}
		@media (max-width: 767px) { 
			.navbar-nav > li > a,
			.navbar-nav > li > a:focus {
			    color: $highlight
			}
			.navbar.fixed .navbar-header,
			.navbar .navbar-header,
			.navbar.fixed .navbar-header {
				background: rgba($header_bg,0.94);
			}
			.navbar .dropdown-menu,
			.navbar.fixed .dropdown-menu {
				background: none;
			}
		}
	";
	wp_add_inline_style( 'ebor-style', $custom_styles );
		
	wp_add_inline_style( 'ebor-style', get_option('custom_css') );
	
}
add_action('wp_enqueue_scripts', 'ebor_load_scripts', 10);

/**
 * Ebor Load Non Standard Scripts
 * Quickly insert HTML into wp_head()
 * @since version 1.0
 * @author TommusRhodus
 */
function ebor_load_non_standard_scripts() {
	echo '<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		  <!--[if lt IE 9]>
			  <script src="'. get_template_directory_uri() . '/style/js/html5shiv.js"></script>
			  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		  <![endif]-->';
}
add_action('wp_head', 'ebor_load_non_standard_scripts', 95);

/**
 * Ebor Load Admin Scripts
 * Properly Enqueues Scripts & Styles for the theme
 * @since version 1.0
 * @author TommusRhodus
 */
function ebor_admin_load_scripts(){
	wp_enqueue_script('ebor-admin-js', get_template_directory_uri().'/admin/admin.js', array('jquery'));
	wp_enqueue_style( 'ebor-admin-css', get_template_directory_uri() . '/admin/css/admin.css' );
	wp_enqueue_style( 'ebor-budicons', get_template_directory_uri() . '/style/type/fonts.css' );
}
add_action('admin_enqueue_scripts', 'ebor_admin_load_scripts', 200);