<?php

class AQ_Page_Header_Block extends AQ_Block {
	
	function __construct() {
		$block_options = array(
			'name' => 'Page Header',
			'size' => 'span12',
			'block_icon' => '<i class="fa fa-camera"></i>',
			'block_description' => 'Use to add a<br />Page Header.'
		);
		parent::__construct('aq_page_header_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'image' => '',
		);
		
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
	?>
		
		<p class="description">
			<label for="<?php echo $this->get_field_id('title') ?>">
				Title
				<?php echo aq_field_input('title', $block_id, $title, $size = 'full') ?>
			</label>
		</p>
		
		<p class="description">
			<label for="<?php echo $this->get_field_id('image') ?>">
				Background Image
				<?php echo aq_field_upload('image', $block_id, $image, $media_type = 'image') ?>
			</label>
		</p>
		
	<?php
	}// end form
	
	function block($instance) {
		extract($instance);
		
		echo ebor_page_title( $title, $image );
		
	}//end block
	
}//end class