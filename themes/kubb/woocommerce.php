<?php 
	/**
	 * woocommerce.php
	 * The default woocommerce page template in Zonya
	 * @author TommusRhodus
	 * @package Zonya
	 * @since 1.0.0
	 */
	get_header(); 
	
	$sidebar = is_active_sidebar('shop');
	$class = ( $sidebar ) ? 'col-sm-8' : 'col-sm-12';
?>

	<div class="offset"></div>
	
<?php	
	/**
	 * Get Wrapper Start - Uses get_template_part for simple child themeing.
	 */
	get_template_part('inc/wrapper','start'); 
?>
	
	<div class="row">
	
		<div class="<?php echo esc_attr($class); ?>">
			<?php woocommerce_content(); ?>
		</div>

		<?php 
			if($sidebar)
				get_sidebar('shop'); 
		?>
	
	</div>
	
<?php
	/**
	 * Get Wrapper End - Uses get_template_part for simple child themeing.
	 */
	get_template_part('inc/wrapper','end'); 
	
	get_footer();
	