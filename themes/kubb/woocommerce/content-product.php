<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;


// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;

$sidebar = is_active_sidebar('shop');
$classes[] = ( $sidebar ) ? 'col-sm-6' : 'col-sm-4';

?>

<div <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<?php
		/**
		 * woocommerce_before_shop_loop_item_title hook
		 *
		 * @hooked woocommerce_template_loop_product_thumbnail - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item_title' );
	?>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="bordered">	
				<div class="row">
	
					<div class="col-md-8">
						<h3><?php the_title(); ?></h3>
						<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
					</div>
					
					<div class="col-md-4">
						<div class="divide10"></div>
						<?php
							/**
							 * woocommerce_after_shop_loop_item_title hook
							 *
							 * @hooked woocommerce_template_loop_rating - 5
							 * @hooked woocommerce_template_loop_price - 10
							 */
							do_action( 'woocommerce_after_shop_loop_item_title' );
						?>
					</div>
			
				</div>
			</div>
		</div>
	</div>

</div>